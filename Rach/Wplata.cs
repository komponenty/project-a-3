﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Microsoft.Win32;

namespace Bank
{
    public partial class Wplata : Form
    {
        Rachunek rach;

        public Wplata()
        {
            InitializeComponent();
            rach = new Rachunek();
            this.Size = new Size(298, 250);
        }

        private void Wplata_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Historia' table. You can move, or remove it, as needed.
            this.historiaTableAdapter.Fill(this.dataSet1.Historia);
            // TODO: This line of code loads data into the 'dataSet1.Rachunek' table. You can move, or remove it, as needed.
            this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);
            // TODO: This line of code loads data into the 'dataSet1.Logowanie' table. You can move, or remove it, as needed.
            this.logowanieTableAdapter.Fill(this.dataSet1.Logowanie);
            string login;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\Bank\Bank");
            login = rk.GetValue("Login").ToString();
            logowanieBindingSource.Filter = "login LIKE '%" + login + "%'";
            ActiveControl = comboBox1;
        }

        private void button3_Click(object sender, EventArgs e) // Wpłata wpisanej kwoty na wybrane konto
        {
            decimal a = decimal.Parse(rachunekDataGridView.CurrentRow.Cells[7].Value.ToString());
            decimal b = decimal.Parse(textBox1.Text);
            rachunekDataGridView.CurrentRow.Cells[7].Value = a + b;
            this.Validate();
            this.rachunekBindingSource.EndEdit();
            this.rachunekTableAdapter.Update(this.dataSet1);
            Historia();
            this.Validate();
            this.historiaBindingSource.EndEdit();
            this.historiaTableAdapter.Update(this.dataSet1);
            this.Close();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e) // Tylko cyfry i przecinek w textBox1
        {
            if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar)
            && e.KeyChar != ',')
            {
                e.Handled = true;
            }
            if (e.KeyChar == ','
                && (sender as TextBox).Text.IndexOf(',') > -1)
            {
                e.Handled = true;
            }
        }

        private void Historia() // Dodanie wiadomości do historii
        {
            historiaBindingSource.AddNew();
            DateTime result = dataDateTimePicker.Value;
            dataDateTimePicker.Text = result.ToString();
            historiaDataGridView.CurrentRow.Cells[1].Value = dataDateTimePicker.Value;
            historiaDataGridView.CurrentRow.Cells[2].Value = "Wpłacono na konto " + comboBox1.Text + " kwotę " + textBox1.Text + " zł. ";
        }

        private void Wplata_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox1.Text != "")
                {
                    button3_Click(sender, e);
                }
            }
        }

        private void Wplata_Paint(object sender, PaintEventArgs e) // Rysowanie tytułu
        {
            Rectangle frameRect = new Rectangle(10, 10, 260, 30);
            LinearGradientBrush background = new LinearGradientBrush(frameRect, Color.White, Color.MediumTurquoise, LinearGradientMode.Vertical);
            e.Graphics.FillPath(background, rach.RoundedRect(frameRect));
            Font fnt = new Font("Verdana", 8, FontStyle.Bold);            
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawString("Wpłata", fnt, new SolidBrush(Color.Black), 120, 20);
        }
    }
}
