﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Microsoft.Win32;

namespace Bank
{
    public partial class Wiadomosci : Form
    {
        Rachunek rach;

        public Wiadomosci()
        {
            InitializeComponent();
            rach = new Rachunek();
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e) // Animowane otwieranie okna
        {
            Top -= 16;
            Left -= 1;
            this.Size += new Size(30, 20);
            if (this.Size.Height >= 500)
            {
                timer1.Enabled = false;
            }
        }

        private void Wiadomosci_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Wiadomosci' table. You can move, or remove it, as needed.
            this.wiadomosciTableAdapter.Fill(this.dataSet1.Wiadomosci);
            // TODO: This line of code loads data into the 'dataSet1.Rachunek' table. You can move, or remove it, as needed.
            this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);
            // TODO: This line of code loads data into the 'dataSet1.Logowanie' table. You can move, or remove it, as needed.
            this.logowanieTableAdapter.Fill(this.dataSet1.Logowanie);
            string login;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\Bank\Bank");
            login = rk.GetValue("Login").ToString();
            logowanieBindingSource.Filter = "login LIKE '%" + login + "%'";
            checkBox1_CheckedChanged(sender, e);
            if (wiadomoscTextBox.Text == "" || wiadomoscTextBox.Text == null)
            {
                przeczytanaCheckBox.Visible = false;
            }
            else
            {
                przeczytanaCheckBox.Visible = true;
            }
        }

        private void Wiadomosci_Paint(object sender, PaintEventArgs e) // Rysowanie tutułu
        {
            Rectangle frameRect = new Rectangle(30, 30, 660, 30);
            LinearGradientBrush background = new LinearGradientBrush(frameRect, Color.White, Color.MediumTurquoise, LinearGradientMode.Vertical);
            e.Graphics.FillPath(background, rach.RoundedRect(frameRect));
            Font fnt = new Font("Verdana", 8, FontStyle.Bold);
            Font fnt2 = new Font("Verdana", 8);          
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawString("Otrzymane wiadomości", fnt, new SolidBrush(Color.Black), 290, 40);
            g.DrawString("Konto", fnt2, new SolidBrush(Color.Black), 33, 98);
            g.DrawString("Temat", fnt2, new SolidBrush(Color.Black), 30, 123);
            g.DrawString("Treść", fnt2, new SolidBrush(Color.Black), 34, 148);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                wiadomosciBindingSource.Filter = "przeczytana = true";
                przeczytanaCheckBox.Visible = false;
            }
            if (checkBox1.Checked == false)
            {
                wiadomosciBindingSource.Filter = "przeczytana = false";
                przeczytanaCheckBox.Visible = true;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void przeczytanaCheckBox_Click(object sender, EventArgs e) // Zapisanie bazy
        {
            this.Validate();
            this.wiadomosciBindingSource.EndEdit();
            this.wiadomosciTableAdapter.Update(this.dataSet1);
            this.wiadomosciTableAdapter.Fill(this.dataSet1.Wiadomosci);
        }

        private void button1_Click(object sender, EventArgs e) // Usuwanie wiadomości
        {
            if (MessageBox.Show("Usunąć ?", "Informacja", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                int a = wiadomosciDataGridView.RowCount;
                int b;
                for (b = 0; b < a; b++)
                {
                    wiadomosciBindingSource.RemoveCurrent();
                }
                this.Validate();
                this.wiadomosciBindingSource.EndEdit();
                this.wiadomosciTableAdapter.Update(this.dataSet1);
            }
        }

        private void wiadomoscTextBox_TextChanged(object sender, EventArgs e)
        {
            if (wiadomoscTextBox.Text == "" || wiadomoscTextBox.Text == null)
            {
                przeczytanaCheckBox.Visible = false;
            }
            else
            {
                przeczytanaCheckBox.Visible = true; 
            }
        }
    }
}
