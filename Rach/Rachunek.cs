﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Microsoft.Win32;
using Rach.Contract;

namespace Bank
{
    public partial class Rachunek : Form, IRachunek
    {
        bool rysuj;
        Kredyt kr;

        public Rachunek()
        {
            InitializeComponent();
            kr = new Kredyt();
            this.Size = new Size(700, 314);
        }

        public GraphicsPath RoundedRect(Rectangle frame) // Zaokrąglanie prostokąta(tło pod tytułem)
        {
            GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
            int radius = 7;
            int diameter = radius * 2;
            Rectangle arc = new Rectangle(frame.Left, frame.Top, diameter, diameter);
            path.AddArc(arc, 180, 90);
            arc.X = frame.Right - diameter;
            path.AddArc(arc, 270, 90);
            arc.Y = frame.Bottom - diameter;
            path.AddArc(arc, 0, 90);
            arc.X = frame.Left;
            path.AddArc(arc, 90, 90);
            path.CloseFigure();
            return path;
        }

        private void Rachunek_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Pozyczka' table. You can move, or remove it, as needed.
            this.pozyczkaTableAdapter.Fill(this.dataSet1.Pozyczka);
            // TODO: This line of code loads data into the 'dataSet1.Historia' table. You can move, or remove it, as needed.
            this.historiaTableAdapter.Fill(this.dataSet1.Historia);
            // TODO: This line of code loads data into the 'dataSet1.Wiadomosci' table. You can move, or remove it, as needed.
            this.wiadomosciTableAdapter.Fill(this.dataSet1.Wiadomosci);
            // TODO: This line of code loads data into the 'dataSet1.Rachunek' table. You can move, or remove it, as needed.
            this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);
            // TODO: This line of code loads data into the 'dataSet1.Logowanie' table. You can move, or remove it, as needed.
            this.logowanieTableAdapter.Fill(this.dataSet1.Logowanie);
            string login;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\Bank\Bank");
            login = rk.GetValue("Login").ToString();
            logowanieBindingSource.Filter = "login LIKE '%" + login + "%'";
            nadaj_Wiadomosc();
        }

        private void Rachunek_Paint(object sender, PaintEventArgs e) // Rysowanie tytułu
        {            
            Rectangle frameRect = new Rectangle(30, 70, 620, 30);
            LinearGradientBrush background = new LinearGradientBrush(frameRect, Color.White, Color.MediumTurquoise, LinearGradientMode.Vertical);
            e.Graphics.FillPath(background, RoundedRect(frameRect));
            Font fnt = new Font("Verdana", 8, FontStyle.Bold);
            Font fnt2 = new Font("Verdana", 12);
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawString("Nr. konta", fnt, new SolidBrush(Color.Black), 160, 82);
            g.DrawString("Lp.", fnt, new SolidBrush(Color.Black), 50, 82);
            g.DrawString("Saldo PLN", fnt, new SolidBrush(Color.Black), 490, 82);
            g.DrawString("Właściciel rachunku :  " + rachunekDataGridView.CurrentRow.Cells[2].Value + " " + rachunekDataGridView.CurrentRow.Cells[1].Value, fnt2, new SolidBrush(Color.Black), 20, 30);
        }

        private void button1_Click(object sender, EventArgs e) // Zakładanie nowego konta
        {
            ZalozRachunek();
        }

        public void ZalozRachunek()
        {
            try
            {
                if (rachunekDataGridView.RowCount < 5)
                {
                    Konto knt = new Konto();
                    knt.nr_Konta();
                    rachunekBindingSource.AddNew();
                    rachunekDataGridView.CurrentRow.Cells[6].Value = knt.numerek_konta;
                    rachunekDataGridView.CurrentRow.Cells[7].Value = 0;
                    rachunekDataGridView.CurrentRow.Cells[1].Value = rachunekDataGridView.Rows[0].Cells[1].Value.ToString();
                    rachunekDataGridView.CurrentRow.Cells[2].Value = rachunekDataGridView.Rows[0].Cells[2].Value.ToString();
                    rachunekDataGridView.CurrentRow.Cells[3].Value = rachunekDataGridView.Rows[0].Cells[3].Value.ToString();
                    rachunekDataGridView.CurrentRow.Cells[4].Value = rachunekDataGridView.Rows[0].Cells[4].Value.ToString();
                    rachunekDataGridView.CurrentRow.Cells[5].Value = rachunekDataGridView.Rows[0].Cells[5].Value.ToString();
                    rachunekDataGridView.CurrentRow.Cells[9].Value = rachunekDataGridView.Rows[0].Cells[9].Value.ToString();
                    rachunekDataGridView.CurrentRow.Cells[10].Value = rachunekDataGridView.Rows[0].Cells[10].Value.ToString();
                    this.Validate();
                    this.rachunekBindingSource.EndEdit();
                    this.rachunekTableAdapter.Update(this.dataSet1);
                    this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);
                    this.Refresh();
                    rysuj = true;
                    Rachunki();
                    Historia();
                    this.Validate();
                    this.historiaBindingSource.EndEdit();
                    this.historiaTableAdapter.Update(this.dataSet1);
                }
                else
                {
                    MessageBox.Show("Możesz posiadać maksymalnie pięć rachunków", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e) // Usuwanie ostatniego konta
        {
            rachunekBindingSource.RemoveCurrent();
            this.Validate();
            this.rachunekBindingSource.EndEdit();
            this.rachunekTableAdapter.Update(this.dataSet1);
            this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);            
        }

        public void Rachunki() // Rysowanie nr.kont
        {
            if (rysuj == true)
            {
                Graphics g = this.CreateGraphics();
                g.SmoothingMode = SmoothingMode.AntiAlias;
                Font fnt = new Font("Verdana", 8);
                int z = rachunekDataGridView.RowCount;
                
                for (int w = 0; w < z; w++)
                {
                    decimal kasa = Convert.ToDecimal(rachunekDataGridView.Rows[w].Cells[7].Value);
                    int n = 110;
                    g.DrawString(w.ToString(), fnt, new SolidBrush(Color.Black), 50, w * 20 + n);
                    g.DrawString(rachunekDataGridView.Rows[w].Cells[6].Value.ToString(), fnt, new SolidBrush(Color.Black), 160, w * 20 + n);
                    g.DrawString(kasa.ToString("C2"), fnt, new SolidBrush(Color.Black), 490, w * 20 + n);
                }
            }
        }

        public void Rachunek_Shown(object sender, EventArgs e)
        {
            Refresh();
            this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);
            rysuj = true;
            Rachunki();
        }

        private void button3_Click(object sender, EventArgs e) // Otwieranie okna wpłaty
        {
            Wplata wp = new Wplata();
            wp.ShowDialog(this);
        }

        private void button4_Click(object sender, EventArgs e) // Odświerzenie okna
        {
            Refresh();
            this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);
            rysuj = true;
            Rachunki();
        }

        private void button5_Click(object sender, EventArgs e) // Otwieranie okna wiadomości
        {
            Bank.Wiadomosci w = new Bank.Wiadomosci();
            w.Show();
        }

        private void timer1_Tick(object sender, EventArgs e) // Sprawdzanie co sek.
        {
            nadaj_Wiadomosc();
            if (dataGridView1.RowCount > 0)
            {
                label1.Visible = true;
                label9.Visible = true;
            }
            else
            {
                label1.Visible = false;
                label9.Visible = false; 
            }
            bool zaznaczenie = false;
            string searchValue = zaznaczenie.ToString();
            int rowIndex = -1;
            foreach (DataGridViewRow row in wiadomosciDataGridView.Rows)
            {
                if (row.Cells[4].Value.ToString().Equals(searchValue))
                {
                    rowIndex = row.Index;
                    wiadomosciDataGridView.Rows[rowIndex].Selected = true;
                    wiadomosciDataGridView.CurrentRow.Selected = false;
                    wiadomosciDataGridView.CurrentCell = wiadomosciDataGridView.Rows[rowIndex].Cells[0];
                    button5.Text = "Nowa Wiadomość";
                    break;
                }
                else
                {
                    button5.Text = "Wiadomości"; 
                }
            }

            try
            {
                DateTime teraz = DateTime.Now;
                DateTime end = DateTime.ParseExact(dataGridView1.CurrentRow.Cells[4].Value.ToString(), "yyyy-MM-dd HH:mm:ss", null);
                dataGridView1.CurrentRow.Cells[5].Value = ((end - teraz).Hours).ToString("00");
                decimal roznica = decimal.Parse(dataGridView1.CurrentRow.Cells[1].Value.ToString()) - decimal.Parse(dataGridView1.CurrentRow.Cells[5].Value.ToString());
                dataGridView1.CurrentRow.Cells[7].Value = roznica;
                Sprawdz();
                label9.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString() + " rat";
            }
            catch { }
        }

        public void Sprawdz() // Obliczanie pozostałego cyklu
        {
            try
            {
                if (decimal.Parse(dataGridView1.CurrentRow.Cells[1].Value.ToString()) <= 0)
                {
                    dataGridView1.CurrentRow.Cells[5].Value = 0;
                    dataGridView1.CurrentRow.Cells[7].Value = 0;
                }
                decimal roznica = decimal.Parse(dataGridView1.CurrentRow.Cells[1].Value.ToString()) - decimal.Parse(dataGridView1.CurrentRow.Cells[5].Value.ToString());
                rachunekDataGridView.CurrentRow.Cells[7].Value = decimal.Parse(rachunekDataGridView.CurrentRow.Cells[7].Value.ToString()) - decimal.Parse(dataGridView1.CurrentRow.Cells[2].Value.ToString()) * roznica;
                dataGridView1.CurrentRow.Cells[1].Value = decimal.Parse(dataGridView1.CurrentRow.Cells[1].Value.ToString()) - roznica;
                dataGridView1.CurrentRow.Cells[1].Value = dataGridView1.CurrentRow.Cells[5].Value;
                if (int.Parse(dataGridView1.CurrentRow.Cells[1].Value.ToString()) <= 0)
                {
                    pozyczkaBindingSource.RemoveCurrent();
                }
                rachunekBindingSource.EndEdit();
                pozyczkaBindingSource.EndEdit();
                tableAdapterManager.UpdateAll(this.dataSet1);
            }
            catch { }
        }

        private void Historia() // Dodanie wiadomości do historii
        {
            historiaBindingSource.AddNew();
            DateTime result = dataDateTimePicker.Value;
            dataDateTimePicker.Text = result.ToString();
            historiaDataGridView.CurrentRow.Cells[1].Value = dataDateTimePicker.Value;
            historiaDataGridView.CurrentRow.Cells[2].Value = "Założono nowy rachunek";           
        }

        public void nadaj_Wiadomosc() // Wiadomość o debecie
        {
            try
            {
                if (decimal.Parse(rachunekDataGridView.CurrentRow.Cells[7].Value.ToString()) < 0)
                {
                    rachunekBindingSource.AddNew();
                    wiadomosciDataGridView.CurrentRow.Cells[1].Value = "Kredyt - Brak środków" + " " + DateTime.Now;
                    wiadomosciDataGridView.CurrentRow.Cells[2].Value = "Konto jest na debecie";
                    wiadomosciDataGridView.CurrentRow.Cells[4].Value = false;
                    rachunekBindingSource.EndEdit();
                    rachunekTableAdapter.Update(this.dataSet1);
                }
            }
            catch { }
        }
    }
}
