﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Bank
{
    public partial class CalaBaza : Form
    {
        
        public CalaBaza()
        {
            InitializeComponent();
            this.Size = new Size(900, 500);
            bindingNavigator1.Renderer = new Renderers.WindowsVistaRenderer();
           
        }

        private void rachunekBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.logowanieBindingSource.EndEdit();
            this.logowanieTableAdapter.Update(this.dataSet1);
        }

        private void CalaBaza_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Logowanie' table. You can move, or remove it, as needed.
            this.logowanieTableAdapter.Fill(this.dataSet1.Logowanie);
            // TODO: This line of code loads data into the 'dataSet1.Rachunek' table. You can move, or remove it, as needed.
            this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.rachunekBindingSource.EndEdit();
            this.rachunekTableAdapter.Update(this.dataSet1);
        }

        private void CalaBaza_Paint(object sender, PaintEventArgs e)
        {
            Rectangle frameRect = new Rectangle(20, 10, 545, 30);
            LinearGradientBrush background = new LinearGradientBrush(frameRect, Color.White, Color.MediumTurquoise, LinearGradientMode.Vertical);
            e.Graphics.FillPath(background, RoundedRect(frameRect));
            Font fnt = new Font("Verdana", 8, FontStyle.Bold);
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawString("Podgląd administratora", fnt, new SolidBrush(Color.Black), 220, 20);
        }

        public GraphicsPath RoundedRect(Rectangle frame)
        {
            GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
            int radius = 7;
            int diameter = radius * 2;
            Rectangle arc = new Rectangle(frame.Left, frame.Top, diameter, diameter);
            path.AddArc(arc, 180, 90);
            arc.X = frame.Right - diameter;
            path.AddArc(arc, 270, 90);
            arc.Y = frame.Bottom - diameter;
            path.AddArc(arc, 0, 90);
            arc.X = frame.Left;
            path.AddArc(arc, 90, 90);
            path.CloseFigure();
            return path;
        }
    }
}
