﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Bank
{
    public partial class Ustawienia : Form
    {
        Rachunek rach;

        public Ustawienia()
        {
            InitializeComponent();
            textBox1.Text = Properties.Settings.Default.Smtp; // Pobieranie zapisanych danych
            textBox2.Text = Properties.Settings.Default.LoginMail;
            textBox3.Text = Properties.Settings.Default.HasloMail;
            textBox4.Text = Properties.Settings.Default.AdresMail;
           
            rach = new Rachunek();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Smtp = textBox1.Text; // Zapisywanie wprowadzonych danych
            Properties.Settings.Default.LoginMail = textBox2.Text;
            Properties.Settings.Default.HasloMail = textBox3.Text;
            Properties.Settings.Default.AdresMail = textBox4.Text;
       
            Properties.Settings.Default.Save();
        }

        private void Ustawienia_Paint(object sender, PaintEventArgs e) // Rysowanie tutułu
        {
            Rectangle frameRect = new Rectangle(10, 10, 327, 30);
            LinearGradientBrush background = new LinearGradientBrush(frameRect, Color.White, Color.MediumTurquoise, LinearGradientMode.Vertical);
            e.Graphics.FillPath(background, rach.RoundedRect(frameRect));
            Font fnt = new Font("Verdana", 8, FontStyle.Bold);
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawString("Ustawienia mail'a", fnt, new SolidBrush(Color.Black), 125, 20);
        }
    }
}
