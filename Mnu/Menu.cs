﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logowanie;
using Microsoft.Win32;

namespace Bank
{
    public partial class Form1 : Form
    {
        Logowanie log;
        Rachunek rach;
        Doladowanie dol;
        Historia hist;
        Przelewy prz;
        Kredyt kr;
        CalaBaza cb;
        Ustawienia ust;

        public Form1()
        {
            InitializeComponent();
            
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.FlatAppearance.BorderSize = 2;
            button1.FlatAppearance.BorderColor = Color.Red;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.FlatAppearance.BorderSize = 0;
            button1.FlatAppearance.BorderColor = Color.White;
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            button2.FlatAppearance.BorderSize = 3;
            button2.FlatAppearance.BorderColor = Color.Red;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.FlatAppearance.BorderSize = 0;
            button2.FlatAppearance.BorderColor = Color.White;
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            button3.FlatAppearance.BorderSize = 3;
            button3.FlatAppearance.BorderColor = Color.Red;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.FlatAppearance.BorderSize = 0;
            button3.FlatAppearance.BorderColor = Color.White;
        }

        private void button4_MouseEnter(object sender, EventArgs e)
        {
            button4.FlatAppearance.BorderSize = 3;
            button4.FlatAppearance.BorderColor = Color.Red;
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            button4.FlatAppearance.BorderSize = 0;
            button4.FlatAppearance.BorderColor = Color.White;
        }

        private void button5_MouseEnter(object sender, EventArgs e)
        {
            button5.FlatAppearance.BorderSize = 3;
            button5.FlatAppearance.BorderColor = Color.Red;
        }

        private void button5_MouseLeave(object sender, EventArgs e)
        {
            button5.FlatAppearance.BorderSize = 0;
            button5.FlatAppearance.BorderColor = Color.White;
        }

        private void button6_MouseLeave(object sender, EventArgs e)
        {
            button6.FlatAppearance.BorderSize = 0;
            button6.FlatAppearance.BorderColor = Color.White;
        }

        private void button6_MouseEnter(object sender, EventArgs e)
        {
            button6.FlatAppearance.BorderSize = 3;
            button6.FlatAppearance.BorderColor = Color.Red;
        }

        private void button6_Click(object sender, EventArgs e) // Wyjście
        {
            log = new Logowanie();
            log.Show();
            this.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            //log.Show();
        }

        private void button1_Click(object sender, EventArgs e) // Okno rachunku
        {
            rach = new Rachunek();
            rach.Show();
        }

        private void Form1_Deactivate(object sender, EventArgs e) // Przezroczystość okna 
        {
            this.Opacity = 0.1;
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            this.Opacity = 1;
        }

        private void button3_Click(object sender, EventArgs e) // Okno doładowania telefonu
        {
            dol = new Doladowanie();
            dol.Show();
        }

        private void button2_Click(object sender, EventArgs e) // Okno z przelewami
        {
            prz = new Przelewy();
            prz.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string login;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\Bank\Bank");
            login = rk.GetValue("Login").ToString(); // Pobranie nazwy z rejestru

            menuStrip1.Renderer = new Renderers.WindowsVistaRenderer();
            if (login == "admin100") // Jeżeli w rejestrze jest wpis "admin100" bedzie widoczna dodatkowa opcja(cała baza)
            {
                ddToolStripMenuItem.Visible = true;
            }
            if (Properties.Settings.Default.Smtp == "" || Properties.Settings.Default.Smtp == null) // Jeżeli rubryka smtp w ustawieniach jest pusta wyswietla komunikat przypominajacy
            {
                MessageBox.Show("Uzupełnij rubryki w ustawieniach", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }

        private void ddToolStripMenuItem_Click(object sender, EventArgs e) // Okno całej bazy
        {
            cb = new CalaBaza();
            cb.Show();
        }

        private void button4_Click(object sender, EventArgs e) // Okno historii
        {
            hist = new Historia();
            hist.Show();
        }

        private void ustawieniaToolStripMenuItem_Click(object sender, EventArgs e) // Oknoo ustawień
        {
            ust = new Ustawienia();
            ust.Show();
        }

        private void button5_Click(object sender, EventArgs e) // Okno kredytów
        {
            kr = new Kredyt();
            kr.Show();
        }
    }
}
