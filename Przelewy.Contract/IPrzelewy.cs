﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Przelewy.Contract
{
    public interface IPrzelewy
    {
        void Przenies_Dane();
        void Sprawdz();
        void Przelew();
        void nadaj_Wiadomosc();
        void odbierz_Wiadomosc();
    }
}
