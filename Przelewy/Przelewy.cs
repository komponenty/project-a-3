﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Microsoft.Win32;
using Przelewy.Contract;

namespace Bank
{
    public partial class Przelewy : Form, IPrzelewy
    {
        CalaBaza cb;
        Font fnt = new Font("Verdana", 8, FontStyle.Bold);
        Font fnt2 = new Font("Verdana", 8);

        public Przelewy()
        {
            InitializeComponent();
            cb = new CalaBaza();
            label2.Text = null;
            this.Size = new Size(500,470);
        }

        private void Przelewy_Paint(object sender, PaintEventArgs e) // Rysowanie tytułu
        {
            Rectangle frameRect = new Rectangle(30, 30, 425, 30);
            LinearGradientBrush background = new LinearGradientBrush(frameRect, Color.White, Color.MediumTurquoise, LinearGradientMode.Vertical);
            e.Graphics.FillPath(background, cb.RoundedRect(frameRect));
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawString("Przelew", fnt, new SolidBrush(Color.Black), 200, 42);
            g.DrawString("Z rachunku :", fnt2, new SolidBrush(Color.Black), 70, 103);
            g.DrawString("Na rachunek :", fnt2, new SolidBrush(Color.Black), 63, 140);
            g.DrawString("Nazwa i adres odbiorcy :", fnt2, new SolidBrush(Color.Black), 5, 173);
            g.DrawString("Tytuł :", fnt2, new SolidBrush(Color.Black), 108, 255);
            g.DrawString("Kwota :", fnt2, new SolidBrush(Color.Black), 101, 335);
        }

        private void Przelewy_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Wiadomosci' table. You can move, or remove it, as needed.
            this.wiadomosciTableAdapter.Fill(this.dataSet1.Wiadomosci);
            // TODO: This line of code loads data into the 'dataSet1.Historia' table. You can move, or remove it, as needed.
            this.historiaTableAdapter.Fill(this.dataSet1.Historia);
            // TODO: This line of code loads data into the 'dataSet1.Rachunek' table. You can move, or remove it, as needed.
            this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);
            // TODO: This line of code loads data into the 'dataSet1.Logowanie' table. You can move, or remove it, as needed.
            this.logowanieTableAdapter.Fill(this.dataSet1.Logowanie);
            // TODO: This line of code loads data into the 'dataSet1.Historia' table. You can move, or remove it, as needed.
            this.historiaTableAdapter.Fill(this.dataSet1.Historia);            
            // TODO: This line of code loads data into the 'dataSet1.Wiadomosci' table. You can move, or remove it, as needed.
            this.wiadomosciTableAdapter.Fill(this.dataSet1.Wiadomosci);
            // TODO: This line of code loads data into the 'dataSet1.Wiadomosci' table. You can move, or remove it, as needed.
            this.wiadomosciTableAdapter.Fill(this.dataSet1.Wiadomosci);
            // TODO: This line of code loads data into the 'dataSet11.Wiadomosci' table. You can move, or remove it, as needed.
            this.wiadomosciTableAdapter.Fill(this.dataSet1.Wiadomosci);
            // TODO: This line of code loads data into the 'dataSet1.Rachunek' table. You can move, or remove it, as needed.
            this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);
            // TODO: This line of code loads data into the 'dataSet1.Logowanie' table. You can move, or remove it, as needed.
            this.logowanieTableAdapter.Fill(this.dataSet1.Logowanie);
            string login;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\Bank\Bank");
            login = rk.GetValue("Login").ToString();
            logowanieBindingSource.Filter = "login LIKE '%" + login + "%'";
            ActiveControl = textBox1;
        }

        public void Przenies_Dane() // Uzupełnia dane jeżeli konto jest z bazy
        {
            textBox2.Text = null;
            textBox2.AppendText(rachunekDataGridView1.CurrentRow.Cells[1].Value.ToString() + " " +
                             rachunekDataGridView1.CurrentRow.Cells[2].Value.ToString() + "\r\n" +
                             rachunekDataGridView1.CurrentRow.Cells[10].Value.ToString() + " , " +
                             rachunekDataGridView1.CurrentRow.Cells[3].Value.ToString());
        }

        private void textBox1_TextChanged(object sender, EventArgs e) // Formatowanie nr konta
        {
            if (textBox1.TextLength == 26 || textBox1.TextLength == 32)
            {
                try
                {
                    string kod = textBox1.Text;
                    ulong a;
                    string result = kod.Substring(0, 14);
                    a = ulong.Parse(result);
                    ulong b;
                    string resultb = kod.Substring(Math.Max(0, kod.Length - 12));
                    b = ulong.Parse(resultb);
                    textBox1.Text = a.ToString("00 0000 0000 0000 ") + b.ToString("0000 0000 0000");
                }
                catch {}
                Sprawdz();                
            }
        }

        public void Sprawdz() // Sprawdza czy konto jest w bazie
        {
            label2.Text = null;
            try
            {
                String searchValue = textBox1.Text;
                int rowIndex = -1;
                foreach (DataGridViewRow row in rachunekDataGridView1.Rows)
                {
                    if (row.Cells[6].Value.ToString().Equals(searchValue))
                    {
                        rowIndex = row.Index;

                        rachunekDataGridView1.Rows[rowIndex].Selected = true;
                        //rachunekDataGridView1.CurrentRow.Selected = false;
                        rachunekDataGridView1.CurrentCell = rachunekDataGridView1.Rows[rowIndex].Cells[0];
                        Przenies_Dane();
                        label2.Text = "Konto jest w bazie";
                        break;
                    }                   
                }               
            }
            catch
            {
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
        && !char.IsDigit(e.KeyChar) && e.KeyChar != ',')
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            textBox1_KeyPress(sender, e);
        }

        private void button2_Click(object sender, EventArgs e) // Czyszczenie rubryk
        {
            textBox1.Text = null;
            textBox2.Text = null;
            textBox3.Text = null;
            textBox4.Text = null;
            label2.Text = null;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = saldoTextBox.Text + " zł";
        }

        public void Przelew() // Przesuwanie kwot pomiędzy tabelami
        {
            decimal a = decimal.Parse(rachunekDataGridView.CurrentRow.Cells[7].Value.ToString());
            decimal b;
            decimal c = decimal.Parse(textBox4.Text);
            if (label2.Text == "Konto jest w bazie")
            {
                b = decimal.Parse(rachunekDataGridView1.CurrentRow.Cells[7].Value.ToString());
                rachunekDataGridView1.CurrentRow.Cells[7].Value = b + c;
            }
            else { b = 0; }
            rachunekDataGridView.CurrentRow.Cells[7].Value = a - c;
            rachunekBindingSource.EndEdit();
            rachunekBindingSource1.EndEdit();
            rachunekTableAdapter.Update(this.dataSet1);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                decimal a = decimal.Parse(textBox4.Text);
                decimal b = decimal.Parse(saldoTextBox.Text);
                if (b < a)
                {
                    MessageBox.Show("Brak środków na koncie !", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    if (MessageBox.Show("Przelać pieniądze ?", "Informacja", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        Przelew();
                        odbierz_Wiadomosc();
                        nadaj_Wiadomosc();
                        wiadomosciTableAdapter.Update(this.dataSet1);
                        Historia();
                        tableAdapterManager.UpdateAll(this.dataSet1);
                        if (label2.Text == "Konto jest w bazie")
                        {

                            MessageBox.Show("Dokonano przelewu.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                        }
                        else
                        {
                            MessageBox.Show("Dokonano przelewu" + "\n" + "na konto spoza bazy.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                        }
                        button2_Click(sender, e);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Uzupełnij.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }

        public void nadaj_Wiadomosc() // Wpis na konto odbiorcy przelewu o ile konto jest w bazie 
        {
            try
            {
                if (label2.Text != null && label2.Text != "")
                {
                    rachunekWiadomosciBindingSource.AddNew();
                    wiadomosciDataGridView1.CurrentRow.Cells[1].Value = "Przelew" + " " + DateTime.Now;
                    wiadomosciDataGridView1.CurrentRow.Cells[2].Value = "Otzymałeś przelew w wysokości " + textBox4.Text + " zł " + "od " + comboBox1.Text + " " +
                        "Tytułem płatności : " + textBox3.Text;
                    wiadomosciDataGridView1.CurrentRow.Cells[4].Value = false;
                    rachunekWiadomosciBindingSource.EndEdit();
                }
            }
            catch { }
        }

        public void odbierz_Wiadomosc() // Wpis wiadomości na konto użytkownika
        {
            try
            {
                wiadomosciBindingSource.AddNew();
                wiadomosciDataGridView.CurrentRow.Cells[1].Value = "Przelew" + " " + DateTime.Now;
                wiadomosciDataGridView.CurrentRow.Cells[2].Value = "Zrobiłeś przelew w wysokości " + textBox4.Text + " zł " + "dla " + textBox1.Text + " " + textBox2.Text + " " +
                    "Tytułem płatności : " + textBox3.Text;
                wiadomosciDataGridView.CurrentRow.Cells[4].Value = false;
                wiadomosciBindingSource.EndEdit();
            }
            catch { }
        }

        private void Historia() // Dodanie wiadomości do historii
        {
            historiaBindingSource.AddNew();
            string result;
            result = DateTime.Now.ToString();
            dataDateTimePicker.Text = result;
            historiaDataGridView.CurrentRow.Cells[1].Value = dataDateTimePicker.Value;
            historiaDataGridView.CurrentRow.Cells[2].Value = "Przelano pieniadzę z konta " + comboBox1.Text + " w kwocie " + textBox4.Text + " zł " + " na konto " + textBox1.Text + ", którego właścicielem jest " + textBox2.Text + ", tytułem " + textBox3.Text + ".";
            historiaBindingSource.EndEdit();
            historiaTableAdapter.Update(this.dataSet1);
        }

        private void Przelewy_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }

    }
}
