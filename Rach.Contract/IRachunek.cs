﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rach.Contract
{
    public interface IRachunek
    {
        void Rachunki();
        void ZalozRachunek();
        void Sprawdz();
        void nadaj_Wiadomosc();
    }
}
