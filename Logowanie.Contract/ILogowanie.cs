﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logowanie.Contract
{
    public interface ILogowanie
    {
        void Rejestr();
        void Zaloguj();
    }
}
