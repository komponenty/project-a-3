﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Bank;
using Logowanie.Contract;
using Nowy_Rachunek;
using Microsoft.Win32;

namespace Bank
{
    public partial class Logowanie : Form, ILogowanie
    {
        Bank.CalaBaza cb;
        Nowy_Rachunek nr;
        Bank.Form1 mnu;
        

        public Logowanie()
        {
            InitializeComponent();
            this.Size = new Size(345, 270);
            cb = new CalaBaza();
            nr = new Nowy_Rachunek();
            mnu = new Form1();
        }
        
        private void Logowanie_Load(object sender, EventArgs e)
        {            
            this.logowanieTableAdapter.Fill(this.dataSet1.Logowanie);
            // TODO: This line of code loads data into the 'dataSet1.Logowanie' table. You can move, or remove it, as needed.
            ActiveControl = txt_login;           
        }

        private void button1_Click(object sender, EventArgs e) // Okno nowego rachunku
        {
            nr = new Nowy_Rachunek();
            nr.Show();
        }
        
        public void Rejestr() // Zakłada rejestr z którego nastepne okna pobieraja dane
        {
            RegistryKey key;
            key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"Software\Bank\Bank");
            key.SetValue("Login", txt_login.Text);
        }

        private void button2_Click(object sender, EventArgs e) // Logowanie
        {
            Zaloguj();
        }

        public void Zaloguj()
        {
            try
            {
                if (txt_login.Text == logowanieDataGridView.CurrentRow.Cells[1].Value.ToString() && txt_haslo.Text == logowanieDataGridView.CurrentRow.Cells[2].Value.ToString())
                {
                    Properties.Settings.Default.login = logowanieDataGridView.CurrentRow.Cells[1].Value.ToString();
                    Rejestr();
                    mnu = new Form1();
                    mnu.Show();
                    this.Hide();

                }
                else
                {
                    MessageBox.Show("Nieprawidłowe dane !", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    ActiveControl = txt_login;
                }
            }
            catch
            {
                MessageBox.Show("Konto o takim loginie nie istnieje !", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
        }

        private void txt_login_TextChanged(object sender, EventArgs e)
        {
            try
            {
                logowanieBindingSource.Filter = "login LIKE '%" + txt_login.Text + "%'";
            }
            catch { }            
        }

        private void Logowanie_Activated(object sender, EventArgs e)
        {
            ActiveControl = txt_login;
            logowanieTableAdapter.Fill(this.dataSet1.Logowanie);
        }

        private void Logowanie_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button2_Click(sender, e);
            }
        }

        private void Logowanie_FormClosing(object sender, FormClosingEventArgs e)
        {           
            Environment.Exit(1);
        }

        private void Logowanie_Paint(object sender, PaintEventArgs e) // Rysowanie tytułu
        {
            Rectangle frameRect = new Rectangle(10, 10, 310, 30);
            LinearGradientBrush background = new LinearGradientBrush(frameRect, Color.White, Color.MediumTurquoise, LinearGradientMode.Vertical);
            e.Graphics.FillPath(background, cb.RoundedRect(frameRect));
            Font fnt = new Font("Verdana", 8, FontStyle.Bold);

            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawString("Logowanie", fnt, new SolidBrush(Color.Black), 130, 20);
        }
    }
}
