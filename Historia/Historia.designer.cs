﻿namespace Bank
{
    partial class Historia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.logowanieDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logowanieBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new Bank.DataSet1();
            this.historiaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.historiaDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button2 = new System.Windows.Forms.Button();
            this.logowanieTableAdapter = new Bank.DataSet1TableAdapters.LogowanieTableAdapter();
            this.tableAdapterManager = new Bank.DataSet1TableAdapters.TableAdapterManager();
            this.historiaTableAdapter = new Bank.DataSet1TableAdapters.HistoriaTableAdapter();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.odbiorcaTextBox = new System.Windows.Forms.TextBox();
            this.tematTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.logowanieDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logowanieBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.historiaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.historiaDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // logowanieDataGridView
            // 
            this.logowanieDataGridView.AutoGenerateColumns = false;
            this.logowanieDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.logowanieDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.logowanieDataGridView.DataSource = this.logowanieBindingSource;
            this.logowanieDataGridView.Location = new System.Drawing.Point(668, 122);
            this.logowanieDataGridView.Name = "logowanieDataGridView";
            this.logowanieDataGridView.Size = new System.Drawing.Size(166, 66);
            this.logowanieDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "lp";
            this.dataGridViewTextBoxColumn1.HeaderText = "lp";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "login";
            this.dataGridViewTextBoxColumn2.HeaderText = "login";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "haslo";
            this.dataGridViewTextBoxColumn3.HeaderText = "haslo";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "lacznik";
            this.dataGridViewTextBoxColumn4.HeaderText = "lacznik";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "lacznikhistoria";
            this.dataGridViewTextBoxColumn5.HeaderText = "lacznikhistoria";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // logowanieBindingSource
            // 
            this.logowanieBindingSource.DataMember = "Logowanie";
            this.logowanieBindingSource.DataSource = this.dataSet1;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // historiaBindingSource
            // 
            this.historiaBindingSource.DataMember = "Logowanie_Historia";
            this.historiaBindingSource.DataSource = this.logowanieBindingSource;
            // 
            // historiaDataGridView
            // 
            this.historiaDataGridView.AllowUserToAddRows = false;
            this.historiaDataGridView.AllowUserToDeleteRows = false;
            this.historiaDataGridView.AllowUserToResizeColumns = false;
            this.historiaDataGridView.AllowUserToResizeRows = false;
            this.historiaDataGridView.AutoGenerateColumns = false;
            this.historiaDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.historiaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.historiaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.historiaDataGridView.DataSource = this.historiaBindingSource;
            this.historiaDataGridView.GridColor = System.Drawing.Color.White;
            this.historiaDataGridView.Location = new System.Drawing.Point(12, 63);
            this.historiaDataGridView.Name = "historiaDataGridView";
            this.historiaDataGridView.RowHeadersVisible = false;
            this.historiaDataGridView.Size = new System.Drawing.Size(921, 406);
            this.historiaDataGridView.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "lp";
            this.dataGridViewTextBoxColumn6.HeaderText = "Lp.";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 80;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "data";
            this.dataGridViewTextBoxColumn7.HeaderText = "Data";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 150;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "tresc";
            this.dataGridViewTextBoxColumn8.HeaderText = "Treść";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.Width = 1500;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "laczniklogowanie";
            this.dataGridViewTextBoxColumn9.HeaderText = "laczniklogowanie";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(858, 482);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Wyslij";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // logowanieTableAdapter
            // 
            this.logowanieTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.HistoriaTableAdapter = this.historiaTableAdapter;
            this.tableAdapterManager.LogowanieTableAdapter = this.logowanieTableAdapter;
            this.tableAdapterManager.RachunekTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = Bank.DataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.WiadomosciTableAdapter = null;
            // 
            // historiaTableAdapter
            // 
            this.historiaTableAdapter.ClearBeforeFill = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 484);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(8, 8);
            this.textBox1.TabIndex = 5;
            this.textBox1.Visible = false;
            // 
            // odbiorcaTextBox
            // 
            this.odbiorcaTextBox.ForeColor = System.Drawing.Color.Silver;
            this.odbiorcaTextBox.Location = new System.Drawing.Point(414, 484);
            this.odbiorcaTextBox.Name = "odbiorcaTextBox";
            this.odbiorcaTextBox.Size = new System.Drawing.Size(242, 20);
            this.odbiorcaTextBox.TabIndex = 6;
            this.odbiorcaTextBox.Text = "Wpisz odbiorcę";
            this.odbiorcaTextBox.TextChanged += new System.EventHandler(this.odbiorcaTextBox_TextChanged);
            this.odbiorcaTextBox.Enter += new System.EventHandler(this.odbiorcaTextBox_Enter);
            this.odbiorcaTextBox.Leave += new System.EventHandler(this.odbiorcaTextBox_Leave);
            // 
            // tematTextBox
            // 
            this.tematTextBox.ForeColor = System.Drawing.Color.Silver;
            this.tematTextBox.Location = new System.Drawing.Point(662, 484);
            this.tematTextBox.Name = "tematTextBox";
            this.tematTextBox.Size = new System.Drawing.Size(190, 20);
            this.tematTextBox.TabIndex = 7;
            this.tematTextBox.Text = "Wpisz temat";
            this.tematTextBox.TextChanged += new System.EventHandler(this.tematTextBox_TextChanged);
            this.tematTextBox.Enter += new System.EventHandler(this.tematTextBox_Enter);
            this.tematTextBox.Leave += new System.EventHandler(this.tematTextBox_Leave);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 482);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Wyczyść historię";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Historia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 517);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tematTextBox);
            this.Controls.Add(this.odbiorcaTextBox);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.historiaDataGridView);
            this.Controls.Add(this.logowanieDataGridView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Historia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Historia_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Historia_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.logowanieDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logowanieBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.historiaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.historiaDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource logowanieBindingSource;
        private DataSet1TableAdapters.LogowanieTableAdapter logowanieTableAdapter;
        private DataSet1TableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView logowanieDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private DataSet1TableAdapters.HistoriaTableAdapter historiaTableAdapter;
        private System.Windows.Forms.BindingSource historiaBindingSource;
        private System.Windows.Forms.DataGridView historiaDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox odbiorcaTextBox;
        private System.Windows.Forms.TextBox tematTextBox;
        private System.Windows.Forms.Button button1;
    }
}