﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Drawing.Drawing2D;
using Microsoft.Win32;
using Historia.Contract;

namespace Bank
{
    public partial class Historia : Form, IHistoria
    {
        Bank.CalaBaza cb;

        public Historia()
        {
            InitializeComponent();
            historiaDataGridView.DefaultCellStyle.SelectionForeColor = Color.Black;
            historiaDataGridView.DefaultCellStyle.SelectionBackColor = Color.White;
            cb = new CalaBaza();
        }

        private void logowanieBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.logowanieBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dataSet1);
        }

        private void Historia_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Historia' table. You can move, or remove it, as needed.
            this.historiaTableAdapter.Fill(this.dataSet1.Historia);
            // TODO: This line of code loads data into the 'dataSet1.Logowanie' table. You can move, or remove it, as needed.
            this.logowanieTableAdapter.Fill(this.dataSet1.Logowanie);
            string login;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\Bank\Bank");
            login = rk.GetValue("Login").ToString();
            logowanieBindingSource.Filter = "login LIKE '%" + login + "%'";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Wyslij();
        }

        public void Wyslij()
        {
            try
            {
                string plik = @"c:\history.txt";
                StreamWriter sW = new StreamWriter(plik);
                int a = historiaDataGridView.RowCount;
                for (int row = 0; row < a; row++)
                {
                    string lines = "";
                    for (int col = 0; col < 4; col++)
                    {
                        lines += (string.IsNullOrEmpty(lines) ? " " : ", ") + historiaDataGridView.Rows[row].Cells[col].Value.ToString();
                    }
                    sW.WriteLine(lines);
                }
                sW.Close();

                textBox1.Text = File.ReadAllText(plik);

                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.Host = Properties.Settings.Default.Smtp;
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(Properties.Settings.Default.LoginMail, Properties.Settings.Default.HasloMail);

                MailMessage mm = new MailMessage(Properties.Settings.Default.AdresMail, odbiorcaTextBox.Text, tematTextBox.Text, textBox1.Text);
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                client.Send(mm);
                MessageBox.Show("Historia została wysłana.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            catch (Exception ex)
            {
                System.Media.SystemSounds.Beep.Play();
                MessageBox.Show(ex.Message);
            }
        }

        private void odbiorcaTextBox_Leave(object sender, EventArgs e)
        {
            if (odbiorcaTextBox.Text == "")
            {
                odbiorcaTextBox.ForeColor = Color.Silver;
                odbiorcaTextBox.Text = "Wpisz odbiorcę";
            }
        }

        private void tematTextBox_Leave(object sender, EventArgs e)
        {
            if (tematTextBox.Text == "")
            {
                tematTextBox.ForeColor = Color.Silver;
                tematTextBox.Text = "Wpisz temat";
            }
        }

        private void odbiorcaTextBox_TextChanged(object sender, EventArgs e)
        {
            if (tematTextBox.Text != "" && odbiorcaTextBox.Text != "")
            {
                button2.Enabled = true;
            }
        }

        private void tematTextBox_TextChanged(object sender, EventArgs e)
        {
            if (tematTextBox.Text != "" && odbiorcaTextBox.Text != "")
            {
                button2.Enabled = true;
            }
        }

        private void odbiorcaTextBox_Enter(object sender, EventArgs e)
        {
            odbiorcaTextBox.ForeColor = Color.Black;
            odbiorcaTextBox.Text = "";
        }

        private void tematTextBox_Enter(object sender, EventArgs e)
        {
            tematTextBox.ForeColor = Color.Black;
            tematTextBox.Text = "";
        }

        private void Historia_Paint(object sender, PaintEventArgs e)
        {
            Rectangle frameRect = new Rectangle(30, 20, 880, 30);
            LinearGradientBrush background = new LinearGradientBrush(frameRect, Color.White, Color.MediumTurquoise, LinearGradientMode.Vertical);
            e.Graphics.FillPath(background, cb.RoundedRect(frameRect));
            Graphics g = e.Graphics;
            Font fnt = new Font("Verdana", 8, FontStyle.Bold);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawString("Historia", fnt, new SolidBrush(Color.Black), 445, 32);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Wyczysc();
        }

        public void Wyczysc()
        {
            if (MessageBox.Show("Wyczyścić ?", "Informacja", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                int a = historiaDataGridView.RowCount;
                int b;
                for (b = 0; b < a; b++)
                {
                    historiaBindingSource.RemoveCurrent();
                }
                this.Validate();
                this.historiaBindingSource.EndEdit();
                this.historiaTableAdapter.Update(this.dataSet1);
            }
        }
    }
}
