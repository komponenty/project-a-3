﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Telefon.Contract
{
    public interface ITelefon
    {
        void Maluj_Kod();
        void Sprawdz_Kod();
        void Doładuj();
    }
}
