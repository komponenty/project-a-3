﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using Telefon.Contract;

namespace Bank
{
    public partial class Doladowanie : Form, ITelefon
    {
        CalaBaza cb;
        Font fnt = new Font("Verdana", 8, FontStyle.Bold);
        Font fnt2 = new Font("Verdana", 8);       
        bool paint;
       
        public Doladowanie()
        {
            InitializeComponent();
            this.Size = new Size(543, 413);
            cb = new CalaBaza();            
        }
        
        private void Doladowanie_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Historia' table. You can move, or remove it, as needed.
            this.historiaTableAdapter.Fill(this.dataSet1.Historia);
            // TODO: This line of code loads data into the 'dataSet1.Rachunek' table. You can move, or remove it, as needed.
            this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);
            // TODO: This line of code loads data into the 'dataSet1.Logowanie' table. You can move, or remove it, as needed.
            this.logowanieTableAdapter.Fill(this.dataSet1.Logowanie);
            string login;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\Bank\Bank");
            login = rk.GetValue("Login").ToString();
            logowanieBindingSource.Filter = "login LIKE '%" + login + "%'";
            comboBox1_SelectedIndexChanged(sender, e);
        }

        private void Doladowanie_Paint(object sender, PaintEventArgs e) // Rysowanie tytułu
        {
            Rectangle frameRect = new Rectangle(30, 30, 470, 30);
            LinearGradientBrush background = new LinearGradientBrush(frameRect, Color.White, Color.MediumTurquoise, LinearGradientMode.Vertical);
            e.Graphics.FillPath(background, cb.RoundedRect(frameRect));
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawString("Nowe doładowanie", fnt, new SolidBrush(Color.Black), 200, 42);
            g.DrawString("Z konta :", fnt2, new SolidBrush(Color.Black), 95, 95);
            g.DrawString("Nr. tel.:", fnt2, new SolidBrush(Color.Black), 103, 130);
            g.DrawString("Kwota doładowania :", fnt2, new SolidBrush(Color.Black), 28, 165);
            g.DrawString("Przepisz kodz obrazka :", fnt2, new SolidBrush(Color.Black), 15, 266); 
        }

        private void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.Select(textBox1.Text.Length, 3);
        }

        private void button1_Click(object sender, EventArgs e) // Odświeżanie rysunku
        {
            pictureBox1.Refresh();
            paint = true;
            Maluj_Kod();
        }

        public void Maluj_Kod() // Rysowanie kodu
        {
            Graphics g = pictureBox1.CreateGraphics();
            Font fnt3 = new Font("Segoe script", 18);
            if (paint == true)
            {
                Rectangle frameRect = new Rectangle(0, 0, 470, 60);
                LinearGradientBrush background = new LinearGradientBrush(frameRect, Color.White, Color.MediumTurquoise, LinearGradientMode.Vertical);
                g.FillPath(background, cb.RoundedRect(frameRect));
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var random = new Random();
                var result = new string(
                    Enumerable.Repeat(chars, 8)
                              .Select(s => s[random.Next(s.Length)])
                              .ToArray());                
                g.DrawString(result, fnt3, new SolidBrush(Color.Black), 17, 5);
                label2.Text = result;
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            button1_Click(sender, e);
            button1.Visible = true;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
             && !char.IsDigit(e.KeyChar)
             && e.KeyChar != '+')
            {
                e.Handled = true;
            } 
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                label1.Text = rachunekDataGridView.CurrentRow.Cells[7].Value.ToString() + " zł";
            }
            catch { }
        }

        public void Sprawdz_Kod() // Sprawdza poprawność przepisania kodu
        {
            string a = label2.Text;
            string b = textBox2.Text;
            if (a == b && textBox1.TextLength == 12)
            {
                button3.Enabled = true;
            }
            else
            {
                button3.Enabled = false;
            } 
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            Sprawdz_Kod();
        }

        private void button3_Click(object sender, EventArgs e) // Odejmowanie z bazy kwoty doladowania
        {
            Doładuj();
        }

        public void Doładuj()
        {
            try
            {
                string s = comboBox2.Text;
                decimal a = decimal.Parse(rachunekDataGridView.CurrentRow.Cells[7].Value.ToString());
                decimal b;
                decimal c;
                var getNumbers = (from t in s
                                  where char.IsDigit(t)
                                  select t).ToArray();
                b = int.Parse(new string(getNumbers));
                if (b <= a)
                {
                    c = a - b;
                    rachunekDataGridView.CurrentRow.Cells[7].Value = c;
                    this.Validate();
                    this.rachunekBindingSource.EndEdit();
                    this.rachunekTableAdapter.Update(this.dataSet1);
                    Historia();
                    this.Validate();
                    this.historiaBindingSource.EndEdit();
                    this.historiaTableAdapter.Update(this.dataSet1);
                    if (MessageBox.Show("Konto zostało doładowane", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.OK)
                    {
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Brak środków na koncie !", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
                }
            }
            catch { }
        }

        public void Historia() // Wiadomość historii
        {
            try
            {
                historiaBindingSource.AddNew();
                DateTime result = dataDateTimePicker.Value;
                dataDateTimePicker.Text = result.ToString();
                historiaDataGridView.CurrentRow.Cells[1].Value = dataDateTimePicker.Value;
                historiaDataGridView.CurrentRow.Cells[2].Value = "Doładowano telefon (" + textBox1.Text + ") kwotą " + comboBox2.Text + " z konta o numerze " + comboBox1.Text + ".";
            }
            catch { }
        }

        private void Doladowanie_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button3_Click(sender, e);
            }
        }
    }
}
