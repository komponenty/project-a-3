﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Drawing.Drawing2D;
using Bank;
using Nowy_Rachunek.Contract;


namespace Bank
{
    public partial class Nowy_Rachunek : Form, INowy_Rachunek
    {
        Bank.CalaBaza log;

        public Nowy_Rachunek()
        {
            InitializeComponent();
            this.Size = new Size(300, 347);
            this.CenterToScreen();
            log = new Bank.CalaBaza();
        }

        private void Nowy_Rachunek_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataSet1.Rachunek' table. You can move, or remove it, as needed.
            this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);
            // TODO: This line of code loads data into the 'dataSet1.Logowanie' table. You can move, or remove it, as needed.
            this.logowanieTableAdapter.Fill(this.dataSet1.Logowanie);
            logowanieBindingSource.AddNew();
        }

        private void button1_Click(object sender, EventArgs e) 
        {
            ZapiszDane();
        }

        public void ZapiszDane() // Jeżeli wszystkie rubryki są uzupełnione to zapisuje dane
        {
            if (imieTextBox.Text != "" && nazwiskoTextBox.Text != "" && adresTextBox.Text != "" && miastoTextBox.Text != "" && peselTextBox.Text != "" && dowodTextBox.Text != "")
            {

                Konto konto = new Konto();
                konto.nr_Konta();
                kontoTextBox.AppendText(konto.numerek_konta);
                DateTime result = dataUrodzeniaDateTimePicker.Value;
                dataUrodzeniaDateTimePicker.Text = result.ToString();
                rachunekDataGridView.CurrentRow.Cells[9].Value = dataUrodzeniaDateTimePicker.Value;
                saldoTextBox.Text = 0.ToString();
                this.Validate();
                rachunekBindingSource.EndEdit();
                rachunekTableAdapter.Update(this.dataSet1);
                if (MessageBox.Show("Nr. konta : " + "\n" + kontoTextBox.Text, "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.OK)
                {
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Uzupełnij dane", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button1);
            }
        }

        private void button2_Click(object sender, EventArgs e) 
        {
            otworzOkno();
        }

        public void otworzOkno() // Otwiera dalszą część okna
        {
            if (this.Size.Width == 300 && hasloTextBox.Text == textBox1.Text)
            {
                if (loginTextBox.Text != "" && hasloTextBox.Text != "")
                {
                    timer1.Enabled = true;
                    button2.Enabled = false;
                    this.Validate();
                    logowanieBindingSource.EndEdit();
                    logowanieTableAdapter.Update(this.dataSet1);
                    rachunekBindingSource.AddNew();
                    ActiveControl = imieTextBox;
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e) // Animacja okna
        {
            this.Left -= 10;
            this.Size += new Size(20, 0);
            if (this.Size.Width == 740)
            {
                timer1.Enabled = false;
            }
        }

        private void loginTextBox_TextChanged(object sender, EventArgs e) 
        {
            sprawdzLogin();
        }

        public void sprawdzLogin()  // Sprawdzanie czy w loginie są litery i cyfry
        {
            string Str = loginTextBox.Text.Trim();
            if (Regex.IsMatch(Str, ".*?[a-zA-Z].*?") && Regex.IsMatch(Str, ".*?[0-9].*?"))
            {
                hasloTextBox.Enabled = true;
                textBox1.Enabled = true;
            }
            else
            {
                hasloTextBox.Enabled = false;
                textBox1.Enabled = false;
            }
        }


        private void Nowy_Rachunek_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (this.Size.Width == 300)
                {
                    button2_Click(sender, e);
                }
                if (this.Size.Width > 300)
                {
                    button1_Click(sender, e);
                }
            }
        }

        private void Nowy_Rachunek_Paint(object sender, PaintEventArgs e) // Rysowanie tytułu
        {
            Rectangle frameRect = new Rectangle(20, 20, 245, 30);
            LinearGradientBrush background = new LinearGradientBrush(frameRect, Color.White, Color.MediumTurquoise, LinearGradientMode.Vertical);
            e.Graphics.FillPath(background, log.RoundedRect(frameRect));
            Font fnt = new Font("Verdana", 8, FontStyle.Bold);
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawString("Nowy rachunek", fnt, new SolidBrush(Color.Black), 90, 30);
        }
    }
}
