﻿namespace Bank
{
    partial class Nowy_Rachunek
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label loginLabel;
            System.Windows.Forms.Label hasloLabel;
            System.Windows.Forms.Label imieLabel;
            System.Windows.Forms.Label nazwiskoLabel;
            System.Windows.Forms.Label adresLabel;
            System.Windows.Forms.Label peselLabel;
            System.Windows.Forms.Label dowodLabel;
            System.Windows.Forms.Label kontoLabel;
            System.Windows.Forms.Label dataUrodzeniaLabel;
            System.Windows.Forms.Label saldoLabel;
            this.dataSet1 = new Bank.DataSet1();
            this.logowanieBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.logowanieTableAdapter = new Bank.DataSet1TableAdapters.LogowanieTableAdapter();
            this.tableAdapterManager = new Bank.DataSet1TableAdapters.TableAdapterManager();
            this.rachunekTableAdapter = new Bank.DataSet1TableAdapters.RachunekTableAdapter();
            this.loginTextBox = new System.Windows.Forms.TextBox();
            this.hasloTextBox = new System.Windows.Forms.TextBox();
            this.rachunekBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imieTextBox = new System.Windows.Forms.TextBox();
            this.nazwiskoTextBox = new System.Windows.Forms.TextBox();
            this.adresTextBox = new System.Windows.Forms.TextBox();
            this.peselTextBox = new System.Windows.Forms.TextBox();
            this.dowodTextBox = new System.Windows.Forms.TextBox();
            this.kontoTextBox = new System.Windows.Forms.TextBox();
            this.dataUrodzeniaDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.miastoTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.saldoTextBox = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.rachunekDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            loginLabel = new System.Windows.Forms.Label();
            hasloLabel = new System.Windows.Forms.Label();
            imieLabel = new System.Windows.Forms.Label();
            nazwiskoLabel = new System.Windows.Forms.Label();
            adresLabel = new System.Windows.Forms.Label();
            peselLabel = new System.Windows.Forms.Label();
            dowodLabel = new System.Windows.Forms.Label();
            kontoLabel = new System.Windows.Forms.Label();
            dataUrodzeniaLabel = new System.Windows.Forms.Label();
            saldoLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logowanieBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rachunekBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rachunekDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // loginLabel
            // 
            loginLabel.AutoSize = true;
            loginLabel.Location = new System.Drawing.Point(60, 96);
            loginLabel.Name = "loginLabel";
            loginLabel.Size = new System.Drawing.Size(32, 13);
            loginLabel.TabIndex = 3;
            loginLabel.Text = "login:";
            // 
            // hasloLabel
            // 
            hasloLabel.AutoSize = true;
            hasloLabel.Location = new System.Drawing.Point(57, 122);
            hasloLabel.Name = "hasloLabel";
            hasloLabel.Size = new System.Drawing.Size(35, 13);
            hasloLabel.TabIndex = 5;
            hasloLabel.Text = "haslo:";
            // 
            // imieLabel
            // 
            imieLabel.AutoSize = true;
            imieLabel.Location = new System.Drawing.Point(353, 53);
            imieLabel.Name = "imieLabel";
            imieLabel.Size = new System.Drawing.Size(32, 13);
            imieLabel.TabIndex = 11;
            imieLabel.Text = "Imię :";
            // 
            // nazwiskoLabel
            // 
            nazwiskoLabel.AutoSize = true;
            nazwiskoLabel.Location = new System.Drawing.Point(326, 79);
            nazwiskoLabel.Name = "nazwiskoLabel";
            nazwiskoLabel.Size = new System.Drawing.Size(59, 13);
            nazwiskoLabel.TabIndex = 13;
            nazwiskoLabel.Text = "Nazwisko :";
            // 
            // adresLabel
            // 
            adresLabel.AutoSize = true;
            adresLabel.Location = new System.Drawing.Point(345, 105);
            adresLabel.Name = "adresLabel";
            adresLabel.Size = new System.Drawing.Size(40, 13);
            adresLabel.TabIndex = 15;
            adresLabel.Text = "Adres :";
            // 
            // peselLabel
            // 
            peselLabel.AutoSize = true;
            peselLabel.Location = new System.Drawing.Point(338, 159);
            peselLabel.Name = "peselLabel";
            peselLabel.Size = new System.Drawing.Size(47, 13);
            peselLabel.TabIndex = 17;
            peselLabel.Text = "PESEL :";
            // 
            // dowodLabel
            // 
            dowodLabel.AutoSize = true;
            dowodLabel.Location = new System.Drawing.Point(317, 185);
            dowodLabel.Name = "dowodLabel";
            dowodLabel.Size = new System.Drawing.Size(68, 13);
            dowodLabel.TabIndex = 19;
            dowodLabel.Text = "Nr. dowodu :";
            // 
            // kontoLabel
            // 
            kontoLabel.AutoSize = true;
            kontoLabel.Location = new System.Drawing.Point(14, 376);
            kontoLabel.Name = "kontoLabel";
            kontoLabel.Size = new System.Drawing.Size(37, 13);
            kontoLabel.TabIndex = 21;
            kontoLabel.Text = "konto:";
            kontoLabel.Visible = false;
            // 
            // dataUrodzeniaLabel
            // 
            dataUrodzeniaLabel.AutoSize = true;
            dataUrodzeniaLabel.Location = new System.Drawing.Point(294, 245);
            dataUrodzeniaLabel.Name = "dataUrodzeniaLabel";
            dataUrodzeniaLabel.Size = new System.Drawing.Size(82, 13);
            dataUrodzeniaLabel.TabIndex = 27;
            dataUrodzeniaLabel.Text = "data Urodzenia:";
            // 
            // saldoLabel
            // 
            saldoLabel.AutoSize = true;
            saldoLabel.Location = new System.Drawing.Point(19, 350);
            saldoLabel.Name = "saldoLabel";
            saldoLabel.Size = new System.Drawing.Size(35, 13);
            saldoLabel.TabIndex = 35;
            saldoLabel.Text = "saldo:";
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // logowanieBindingSource
            // 
            this.logowanieBindingSource.DataMember = "Logowanie";
            this.logowanieBindingSource.DataSource = this.dataSet1;
            // 
            // logowanieTableAdapter
            // 
            this.logowanieTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.HistoriaTableAdapter = null;
            this.tableAdapterManager.LogowanieTableAdapter = this.logowanieTableAdapter;
            this.tableAdapterManager.PozyczkaTableAdapter = null;
            this.tableAdapterManager.RachunekTableAdapter = this.rachunekTableAdapter;
            this.tableAdapterManager.UpdateOrder = Bank.DataSet1TableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.WiadomosciTableAdapter = null;
            // 
            // rachunekTableAdapter
            // 
            this.rachunekTableAdapter.ClearBeforeFill = true;
            // 
            // loginTextBox
            // 
            this.loginTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.logowanieBindingSource, "login", true));
            this.loginTextBox.Location = new System.Drawing.Point(98, 93);
            this.loginTextBox.Name = "loginTextBox";
            this.loginTextBox.Size = new System.Drawing.Size(161, 20);
            this.loginTextBox.TabIndex = 1;
            this.toolTip1.SetToolTip(this.loginTextBox, "Musi zawierać litery i cyfry");
            this.loginTextBox.TextChanged += new System.EventHandler(this.loginTextBox_TextChanged);
            // 
            // hasloTextBox
            // 
            this.hasloTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.logowanieBindingSource, "haslo", true));
            this.hasloTextBox.Enabled = false;
            this.hasloTextBox.Location = new System.Drawing.Point(98, 119);
            this.hasloTextBox.Name = "hasloTextBox";
            this.hasloTextBox.PasswordChar = '•';
            this.hasloTextBox.Size = new System.Drawing.Size(161, 20);
            this.hasloTextBox.TabIndex = 2;
            // 
            // rachunekBindingSource
            // 
            this.rachunekBindingSource.DataMember = "Logowanie_Rachunek";
            this.rachunekBindingSource.DataSource = this.logowanieBindingSource;
            // 
            // imieTextBox
            // 
            this.imieTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rachunekBindingSource, "imie", true));
            this.imieTextBox.Location = new System.Drawing.Point(391, 50);
            this.imieTextBox.Name = "imieTextBox";
            this.imieTextBox.Size = new System.Drawing.Size(223, 20);
            this.imieTextBox.TabIndex = 5;
            // 
            // nazwiskoTextBox
            // 
            this.nazwiskoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rachunekBindingSource, "nazwisko", true));
            this.nazwiskoTextBox.Location = new System.Drawing.Point(391, 76);
            this.nazwiskoTextBox.Name = "nazwiskoTextBox";
            this.nazwiskoTextBox.Size = new System.Drawing.Size(223, 20);
            this.nazwiskoTextBox.TabIndex = 6;
            // 
            // adresTextBox
            // 
            this.adresTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rachunekBindingSource, "adres", true));
            this.adresTextBox.Location = new System.Drawing.Point(391, 130);
            this.adresTextBox.Name = "adresTextBox";
            this.adresTextBox.Size = new System.Drawing.Size(223, 20);
            this.adresTextBox.TabIndex = 8;
            // 
            // peselTextBox
            // 
            this.peselTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rachunekBindingSource, "pesel", true));
            this.peselTextBox.Location = new System.Drawing.Point(391, 156);
            this.peselTextBox.Name = "peselTextBox";
            this.peselTextBox.Size = new System.Drawing.Size(223, 20);
            this.peselTextBox.TabIndex = 9;
            // 
            // dowodTextBox
            // 
            this.dowodTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rachunekBindingSource, "dowod", true));
            this.dowodTextBox.Location = new System.Drawing.Point(391, 182);
            this.dowodTextBox.Name = "dowodTextBox";
            this.dowodTextBox.Size = new System.Drawing.Size(223, 20);
            this.dowodTextBox.TabIndex = 10;
            // 
            // kontoTextBox
            // 
            this.kontoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rachunekBindingSource, "konto", true));
            this.kontoTextBox.Location = new System.Drawing.Point(60, 373);
            this.kontoTextBox.MaxLength = 9999999;
            this.kontoTextBox.Name = "kontoTextBox";
            this.kontoTextBox.Size = new System.Drawing.Size(223, 20);
            this.kontoTextBox.TabIndex = 22;
            // 
            // dataUrodzeniaDateTimePicker
            // 
            this.dataUrodzeniaDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.rachunekBindingSource, "dataUrodzenia", true));
            this.dataUrodzeniaDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dataUrodzeniaDateTimePicker.Location = new System.Drawing.Point(391, 241);
            this.dataUrodzeniaDateTimePicker.Name = "dataUrodzeniaDateTimePicker";
            this.dataUrodzeniaDateTimePicker.Size = new System.Drawing.Size(223, 20);
            this.dataUrodzeniaDateTimePicker.TabIndex = 11;
            this.dataUrodzeniaDateTimePicker.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // miastoTextBox
            // 
            this.miastoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rachunekBindingSource, "miasto", true));
            this.miastoTextBox.Location = new System.Drawing.Point(391, 102);
            this.miastoTextBox.Name = "miastoTextBox";
            this.miastoTextBox.Size = new System.Drawing.Size(223, 20);
            this.miastoTextBox.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(620, 241);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 46);
            this.button1.TabIndex = 12;
            this.button1.Text = "Zapisz";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(98, 204);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(161, 40);
            this.button2.TabIndex = 4;
            this.button2.Text = "Dalej";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(98, 145);
            this.textBox1.Name = "textBox1";
            this.textBox1.PasswordChar = '•';
            this.textBox1.Size = new System.Drawing.Size(161, 20);
            this.textBox1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 148);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "powtórz hasło :";
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // saldoTextBox
            // 
            this.saldoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.rachunekBindingSource, "saldo", true));
            this.saldoTextBox.Location = new System.Drawing.Point(60, 347);
            this.saldoTextBox.Name = "saldoTextBox";
            this.saldoTextBox.Size = new System.Drawing.Size(223, 20);
            this.saldoTextBox.TabIndex = 36;
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 5;
            this.toolTip1.ReshowDelay = 100;
            // 
            // rachunekDataGridView
            // 
            this.rachunekDataGridView.AutoGenerateColumns = false;
            this.rachunekDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.rachunekDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rachunekDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11});
            this.rachunekDataGridView.DataSource = this.rachunekBindingSource;
            this.rachunekDataGridView.Location = new System.Drawing.Point(297, 347);
            this.rachunekDataGridView.Name = "rachunekDataGridView";
            this.rachunekDataGridView.RowHeadersVisible = false;
            this.rachunekDataGridView.Size = new System.Drawing.Size(415, 79);
            this.rachunekDataGridView.TabIndex = 36;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "lp";
            this.dataGridViewTextBoxColumn1.HeaderText = "lp";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "imie";
            this.dataGridViewTextBoxColumn2.HeaderText = "imie";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "nazwisko";
            this.dataGridViewTextBoxColumn3.HeaderText = "nazwisko";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "adres";
            this.dataGridViewTextBoxColumn4.HeaderText = "adres";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "pesel";
            this.dataGridViewTextBoxColumn5.HeaderText = "pesel";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "dowod";
            this.dataGridViewTextBoxColumn6.HeaderText = "dowod";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "konto";
            this.dataGridViewTextBoxColumn7.HeaderText = "konto";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "saldo";
            this.dataGridViewTextBoxColumn8.HeaderText = "saldo";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "laczniklogowanie";
            this.dataGridViewTextBoxColumn9.HeaderText = "laczniklogowanie";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "dataUrodzenia";
            this.dataGridViewTextBoxColumn10.HeaderText = "dataUrodzenia";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "miasto";
            this.dataGridViewTextBoxColumn11.HeaderText = "miasto";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // Nowy_Rachunek
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(732, 450);
            this.Controls.Add(this.rachunekDataGridView);
            this.Controls.Add(saldoLabel);
            this.Controls.Add(this.saldoTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(imieLabel);
            this.Controls.Add(this.imieTextBox);
            this.Controls.Add(nazwiskoLabel);
            this.Controls.Add(this.nazwiskoTextBox);
            this.Controls.Add(adresLabel);
            this.Controls.Add(this.adresTextBox);
            this.Controls.Add(peselLabel);
            this.Controls.Add(this.peselTextBox);
            this.Controls.Add(dowodLabel);
            this.Controls.Add(this.dowodTextBox);
            this.Controls.Add(kontoLabel);
            this.Controls.Add(this.kontoTextBox);
            this.Controls.Add(dataUrodzeniaLabel);
            this.Controls.Add(this.dataUrodzeniaDateTimePicker);
            this.Controls.Add(this.miastoTextBox);
            this.Controls.Add(loginLabel);
            this.Controls.Add(this.loginTextBox);
            this.Controls.Add(hasloLabel);
            this.Controls.Add(this.hasloTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.KeyPreview = true;
            this.Name = "Nowy_Rachunek";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Nowy_Rachunek_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Nowy_Rachunek_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Nowy_Rachunek_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logowanieBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rachunekBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rachunekDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bank.DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource logowanieBindingSource;
        private DataSet1TableAdapters.LogowanieTableAdapter logowanieTableAdapter;
        private DataSet1TableAdapters.TableAdapterManager tableAdapterManager;
        private DataSet1TableAdapters.RachunekTableAdapter rachunekTableAdapter;
        private System.Windows.Forms.TextBox loginTextBox;
        private System.Windows.Forms.TextBox hasloTextBox;
        private System.Windows.Forms.BindingSource rachunekBindingSource;
        private System.Windows.Forms.TextBox imieTextBox;
        private System.Windows.Forms.TextBox nazwiskoTextBox;
        private System.Windows.Forms.TextBox adresTextBox;
        private System.Windows.Forms.TextBox peselTextBox;
        private System.Windows.Forms.TextBox dowodTextBox;
        private System.Windows.Forms.TextBox kontoTextBox;
        private System.Windows.Forms.DateTimePicker dataUrodzeniaDateTimePicker;
        private System.Windows.Forms.TextBox miastoTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox saldoTextBox;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridView rachunekDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
    }
}