﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pozyczka.Contract
{
    public interface IKredyt
    {
        void Pozyczka();
        void DodajKredyt();
        void Sprawdz();
    }
}
