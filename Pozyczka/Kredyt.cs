﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using Microsoft.Win32;
using Pozyczka.Contract;

namespace Bank
{
    public partial class Kredyt : Form, IKredyt
    {
        CalaBaza cb;

        public Kredyt()
        {
            InitializeComponent();
            cb = new CalaBaza();
            this.Size = new Size(433, 350);
        }

        private void Kredyt_Load(object sender, EventArgs e) /// Ładowanie formy
        {
            // TODO: This line of code loads data into the 'dataSet1.Historia' table. You can move, or remove it, as needed.
            this.historiaTableAdapter.Fill(this.dataSet1.Historia);
            // TODO: This line of code loads data into the 'dataSet1.Wiadomosci' table. You can move, or remove it, as needed.
            this.wiadomosciTableAdapter.Fill(this.dataSet1.Wiadomosci);
            // TODO: This line of code loads data into the 'dataSet1.Pozyczka' table. You can move, or remove it, as needed.
            this.pozyczkaTableAdapter.Fill(this.dataSet1.Pozyczka);
            // TODO: This line of code loads data into the 'dataSet1.Rachunek' table. You can move, or remove it, as needed.
            this.rachunekTableAdapter.Fill(this.dataSet1.Rachunek);
            // TODO: This line of code loads data into the 'dataSet1.Logowanie' table. You can move, or remove it, as needed.
            this.logowanieTableAdapter.Fill(this.dataSet1.Logowanie);
            string login;
            RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"Software\Bank\Bank");
            login = rk.GetValue("Login").ToString();
            logowanieBindingSource.Filter = "login LIKE '%" + login + "%'";
            if (pozyczkaDataGridView.RowCount > 0)
            {
                label8.Visible = true;
                label9.Visible = true;
            }
        } 

        public void Pozyczka()
        {
            if (pozyczkaDataGridView.RowCount > 0) // Sprawdza czy są jakieś zaciagniete pożyczki
            {
                pozyczkaBindingSource.RemoveCurrent(); // Usuwa jeżeli są jakieś pozostałości
            }
            pozyczkaBindingSource.AddNew(); // Dodaje nowy wiersz do bazy
            pozyczkaDataGridView.CurrentRow.Cells[1].Value = comboBox1.Text; // Dodaje do bazy ilość rat
            pozyczkaDataGridView.CurrentRow.Cells[3].Value = DateTime.Now; // Dodaje początkowy/aktualny czas
            pozyczkaDataGridView.CurrentRow.Cells[4].Value = DateTime.Now.AddHours(int.Parse(comboBox1.Text)); // Dodaje do czasu końcowego godziny(ilość rat)
            pozyczkaDataGridView.CurrentRow.Cells[2].Value = label2.Text;
            rachunekDataGridView.CurrentRow.Cells[7].Value = decimal.Parse(rachunekDataGridView.CurrentRow.Cells[7].Value.ToString()) + decimal.Parse(comboBox2.Text); // Dodanie pożyczki do salda
            pozyczkaBindingSource.EndEdit();
            pozyczkaTableAdapter.Update(this.dataSet1);
            rachunekBindingSource.EndEdit();
            rachunekTableAdapter.Update(this.dataSet1);
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DodajKredyt();
        }

        public void DodajKredyt()
        {
            if (pozyczkaDataGridView.RowCount > 0)
            {
                MessageBox.Show("Możesz posiadać tylko jeden kredyt", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                Pozyczka();
                Historia();
                if (MessageBox.Show("Dostałeś pożyczkę w wysokości " + comboBox2.Text + " zł", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.OK)
                {
                    this.Close();
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e) // Przeliczanie czasu
        {
            try
            {
                DateTime teraz = DateTime.Now;
                DateTime end = DateTime.ParseExact(pozyczkaDataGridView.CurrentRow.Cells[4].Value.ToString(), "yyyy-MM-dd HH:mm:ss", null);
                pozyczkaDataGridView.CurrentRow.Cells[5].Value = ((end - teraz).Hours).ToString("00");
                decimal roznica = decimal.Parse(pozyczkaDataGridView.CurrentRow.Cells[1].Value.ToString()) - decimal.Parse(pozyczkaDataGridView.CurrentRow.Cells[5].Value.ToString());
                pozyczkaDataGridView.CurrentRow.Cells[7].Value = roznica;
                Sprawdz();                
                label9.Text = pozyczkaDataGridView.CurrentRow.Cells[1].Value.ToString() + " rat"; 
            }
            catch { }
        }

        public void Sprawdz() // Obliczanie pozostałego cyklu
        {
            try
            {
                if (decimal.Parse(pozyczkaDataGridView.CurrentRow.Cells[1].Value.ToString()) <= 0)
                {
                    pozyczkaDataGridView.CurrentRow.Cells[5].Value = 0;
                    pozyczkaDataGridView.CurrentRow.Cells[7].Value = 0;
                }
                decimal roznica = decimal.Parse(pozyczkaDataGridView.CurrentRow.Cells[1].Value.ToString()) - decimal.Parse(pozyczkaDataGridView.CurrentRow.Cells[5].Value.ToString());
                rachunekDataGridView.CurrentRow.Cells[7].Value = decimal.Parse(rachunekDataGridView.CurrentRow.Cells[7].Value.ToString()) - decimal.Parse(pozyczkaDataGridView.CurrentRow.Cells[2].Value.ToString()) * roznica;
                pozyczkaDataGridView.CurrentRow.Cells[1].Value = decimal.Parse(pozyczkaDataGridView.CurrentRow.Cells[1].Value.ToString()) - roznica;
                pozyczkaDataGridView.CurrentRow.Cells[1].Value = pozyczkaDataGridView.CurrentRow.Cells[5].Value;
                if (int.Parse(pozyczkaDataGridView.CurrentRow.Cells[1].Value.ToString()) <= 0)
                {
                    pozyczkaBindingSource.RemoveCurrent();
                }
                rachunekBindingSource.EndEdit();
                pozyczkaBindingSource.EndEdit();
                tableAdapterManager.UpdateAll(this.dataSet1);
            }
            catch { }
        }

        private void button2_Click_1(object sender, EventArgs e) // Obliczanie całkowitej kwoty pożyczki oraz raty pobieranej co godzinę
        {
            try
            {
                decimal kwota = decimal.Parse(comboBox2.Text);
                decimal procent = decimal.Parse((0.07).ToString());
                decimal kwotakredytu = kwota * procent + kwota;
                label1.Text = kwotakredytu.ToString("C2");
                decimal raty = decimal.Parse(comboBox1.Text);
                label2.Text = (kwotakredytu / raty).ToString();
                decimal kasa = decimal.Parse(label2.Text);
                label3.Text = kasa.ToString("C2");
                button1.Enabled = true;
            }
            catch { }
        }

        private void saldoLabel1_TextChanged(object sender, EventArgs e) // Zamiana labela z bazy na formatowany label w zł
        {
            try
            {
                decimal kasa = decimal.Parse(saldoLabel1.Text);
                label6.Text = kasa.ToString("C2");
            }
            catch { }
        }

        private void comboBox2_DropDown(object sender, EventArgs e) // Dodawanie do comboBox kwot zależne od posiadanego salda
        {
            
            if (decimal.Parse(saldoLabel1.Text) <= 10000)
            {
                comboBox2.Items.Clear();
                comboBox2.Items.Add(20000);
            }
            if (decimal.Parse(saldoLabel1.Text) <= 20000 && decimal.Parse(saldoLabel1.Text) >= 10000)
            {
                comboBox2.Items.Clear();
                comboBox2.Items.Add(20000);
                comboBox2.Items.Add(50000);
            }
            if (decimal.Parse(saldoLabel1.Text) >= 20000)
            {
                comboBox2.Items.Clear();
                comboBox2.Items.Add(20000);
                comboBox2.Items.Add(50000);
                comboBox2.Items.Add(400000);
            }
        }

        private void Historia() // Dodanie wiadomości do historii
        {
            historiaBindingSource.AddNew();
            string result;
            result = DateTime.Now.ToString();
            dataDateTimePicker.Text = result;
            historiaDataGridView.CurrentRow.Cells[1].Value = dataDateTimePicker.Value;
            historiaDataGridView.CurrentRow.Cells[2].Value = "Otzymałeś kredyt w wysokości " + comboBox2.Text + " zł ";
            historiaBindingSource.EndEdit();
            historiaTableAdapter.Update(this.dataSet1);
        }

        private void Kredyt_Paint(object sender, PaintEventArgs e) // Rysowanie tytułu
        {
            Rectangle frameRect = new Rectangle(20, 20, 370, 30);
            LinearGradientBrush background = new LinearGradientBrush(frameRect, Color.White, Color.MediumTurquoise, LinearGradientMode.Vertical);
            e.Graphics.FillPath(background, cb.RoundedRect(frameRect));
            Font fnt = new Font("Verdana", 8, FontStyle.Bold);
            Graphics g = e.Graphics;
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.DrawString("Kredyty", fnt, new SolidBrush(Color.Black), 170, 32);
        }
    }
}
